# Create your views here.
#coded by arnab
from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate,logout
from django.contrib.auth.models import User
from ribbit_app.forms import AuthenticationForm,UserCreateForm, RibbitForm, AuthenticateForm
from ribbit_app.models import ribbit2

from django.contrib.auth.decorators import login_required


def index(request, auth_form=None, user_form=None):

    if request.user.is_authenticated():
        ribbit_form= RibbitForm()
        user = request.user
        ribbits_self= ribbit2.objects.filter(user=user.id)
        ribbit_buddies= ribbit2.objects.filter(user__userprofile__in=user.profile.follows.all)
        ribbits = ribbits_self | ribbit_buddies

        return render(request,
        'buddies.html',
        {'ribbit_form': ribbit_form , 'user': user,
        #'ribbits2': ribbits2,
        'ribbits': ribbits,
        'next_url' : '/'}),



    else:

        auth_form = auth_form or AuthenticateForm()
        user_form = user_form or UserCreateForm()

        return render(request, 'home.html', {'auth_form': auth_form,'user_form': user_form})


def login_view(request):
    if request.method =='POST':
        form= AuthenticateForm(data=request.POST)
        if form.is_valid():
            login(request,form.get_user())
            return redirect('/')
        else:
            return index(request,auth_form=form)
    return redirect('/')


def logout_view(request):
    logout(request)
    return redirect('/')


def signup(request):
    user_form= UserCreateForm(data=request.POST)
    if request.method =='POST':
        if user_form.is_valid():
            username =user_form.clean_username()
            password =user_form.clean_password2()
            user_form.save()
            user = authenticate(username= username, password=password)
            logout(request,user)
            return index(request,user_form=user_form)
        return redirect('/')



from django.db.models import Count
from django.http import  Http404
'''
def get_latest(user):
    try:
        return user.ribbit_set.order_by('-id')[0]
    except IndexError:
        return ""
@login_required
def users(request, username="", ribbit_form=None):
    if username:
        # Show a profile
        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            raise Http404
        ribbits = Ribbit.objects.filter(user=user.id)
        if username == request.user.username or request.user.profile.follows.filter(user__username=username):
            # Self Profile or buddies' profile
            return render(request, 'user.html', {'user': user, 'ribbits': ribbits, })
        return render(request, 'user.html', {'user': user, 'ribbits': ribbits, 'follow': True, })
    users = User.objects.all().annotate(ribbit_count=Count('ribbit'))
    ribbits = map(get_latest, users)
    obj = zip(users, ribbits)
    ribbit_form = ribbit_form or RibbitForm()
    return render(request,
        'profiles.html',
        {'obj': obj, 'next_url': '/users/',
         'ribbit_form': ribbit_form,
         'username': request.user.username, })



def get_latest(user):
    try:
        return  user.ribbit_set.order_by('-id')[0]
    except IndexError:
        return  ""

@login_required

def users(request, username="", ribbit_form=None):
    if username:
        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            raise Http404
        ribbits= Ribbit.objects.filter(user=user.id)
        if username == request.user.username or request.user.profile.follows.filter(user__username=username):


            return render(request,'user.html',{'user':user, 'ribbits': ribbits, })
        return render(request, 'user.html', {'user': user, 'ribbits': ribbits, 'follow': True, })
        #return render(request, 'user.html',{'user': user, 'ribbits': ribbits, 'follow' = True, })
    users = User.objects.all().annotate(ribbit_count=Count('ribbit'))
    #users = User.objects.all().annotate(ribbit_form=Count('ribbits'))
    ribbits=map(get_latest,users)
    obj=zip(users, ribbits )
    ribbit_form= ribbit_form or RibbitForm()
    return render(request,
            'profiles.html',
            {'obj': obj, 'next_url': '/users/',
            'ribbit_form': ribbit_form,
            'username': request.user.username, })     #jhamela ase
 '''

from django.db.models import Count
from django.http import Http404
def get_latest(user):
    try:
        return user.ribbit_set.order_by('-id')[0]
    except IndexError:
        return ""


@login_required
def users(request, username="", ribbit_form=None):
    if username:
        # Show a profile
        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            raise Http404
        ribbits = ribbit2.objects.filter(user=user.id)
        if username == request.user.username or request.user.profile.follows.filter(user__username=username):
            # Self Profile or buddies' profile
            return render(request, 'user.html', {'user': user, 'ribbits': ribbits, })
        return render(request, 'user.html', {'user': user, 'ribbits': ribbits, 'follow': True, })
    users = User.objects.all().annotate(ribbit_count=Count('ribbit'))
    ribbits = map(get_latest, users)
    obj = zip(users, ribbits)
    ribbit_form = ribbit_form or RibbitForm()
    return render(request,
        'profiles.html',
        {'obj': obj, 'next_url': '/users/',
         'ribbit_form': ribbit_form,
         'username': request.user.username, })
from django.core.exceptions import ObjectDoesNotExist

@login_required

def follow(request):
    if request.method == "POST":
        follow_id = request.POST.get('follow',False)
        if follow_id:
            try:
                user = User.objects.get(id=follow_id)
                request.user.profile.follows.add(user.profile)
            except ObjectDoesNotExist:
                return redirect('/user/')
    return  redirect('user')



